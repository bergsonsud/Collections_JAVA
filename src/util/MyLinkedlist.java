package util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import br.com.fametro.entidades.Pessoa;

public class MyLinkedlist {
	
	List<Pessoa> listaPessoas = new LinkedList<Pessoa>();
	static long totais[] = new long[3];
	long ini;
	long tempoTotal;
	
	

	public LinkedList<Pessoa> construirPessoa(){
		LinkedList<Pessoa> list = new LinkedList<Pessoa>();
		for (int i = 1; i <= new Random().nextInt(99999)*999; i++) {
			
			Pessoa pessoa1 = new Pessoa();
			pessoa1.setCpf(new Random().nextInt(1000)*100);
			
			SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/YYYY");			
			Pessoa pessoa = criarPessoa("Pessoa"+i,pessoa1.hashCode(),dt1.format(new Date()),"Mae"+i);
			list.add(pessoa);
		}		
		
		return list;
		
	}
	
	
	
	public Pessoa criarPessoa(String nome,int cpf, String dataNascimento, String nomeMae){
		Pessoa pessoa = new Pessoa();
		pessoa.setNome(nome);
		pessoa.setCpf(cpf);
		pessoa.setDataNascimento(dataNascimento);
		pessoa.setNomeMae(nomeMae);
		
		return pessoa;
	}
	
	
	
	public void processar(){
		ini = System.currentTimeMillis();
		listaPessoas = construirPessoa();
		
		for (int i = 1; i < listaPessoas.size(); i++) {
			Pessoa pessoa = listaPessoas.get(i);			
			imp(pessoa);			
		}


		terminaContagem(0);
	}
	
	public boolean contains(String cpf){
		ini = System.currentTimeMillis();
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setCpf(Integer.parseInt(cpf));
		
		
		cpf = ""+pessoa1.hashCode(); 

		for (Pessoa pessoa: listaPessoas ) {
			
						
			if (cpf == ""+pessoa.getCpf()) {	
				
				terminaContagem(1);
				System.out.println("Achou CPF:"+cpf);
				return true;
				
			}
		}
		terminaContagem(1);
		System.out.println("Não Achou CPF:"+cpf);
		return false;
	}
	

	public void remover(String cpf){
		ini = System.currentTimeMillis();
		
		for (int i = 0; i < listaPessoas.size(); i++) {
			Pessoa pessoa = listaPessoas.get(i);
			if (cpf == ""+pessoa.getCpf()) {
			
				listaPessoas.remove(i);				
			}
			terminaContagem(2);	
		}

		
	}
	
	
	public void imp(Pessoa pessoa){
		System.out.println("Nome: "+pessoa.getNome());
		System.out.println("CPF: "+pessoa.getCpf());
		System.out.println("Data Nascimento: "+pessoa.getDataNascimento());
		System.out.println("Nome da Mae: "+pessoa.getNomeMae());
		System.out.println("***************");	
		
	}
	
	public static void impTot(){
		System.out.println("\n-----------");
		System.out.println("LinkedList");
		System.out.println("-----------");
		System.out.println("Adição: "+totais[0]+" ms");
		System.out.println("Contains: "+totais[1]+" ms");
		System.out.println("Remover: "+totais[2]+" ms");
		System.out.println("-----------");
	}
	
	public void terminaContagem(int n){
		tempoTotal = System.currentTimeMillis() - ini;
		totais[n]=tempoTotal;
	}
	

}
