package util;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Random;

import br.com.fametro.entidades.Pessoa;


public class MyHashset{
	
	
	  HashSet<Pessoa> listaPessoas = new HashSet<Pessoa>();   
	
	static long totais[] = new long[3];
	static long ini;
	static long tempoTotal;
	
	
	public static HashSet<Pessoa> construirPessoa(){
		 HashSet<Pessoa> list = new HashSet<Pessoa>(); 
		for (int i = 1; i <= new Random().nextInt(99999)*999; i++) {
			
			Pessoa pessoa1 = new Pessoa();
			pessoa1.setCpf(new Random().nextInt(1000)*100);
			
			SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/YYYY");			
			Pessoa pessoa = criarPessoa("Pessoa"+i,pessoa1.hashCode(),dt1.format(new Date()),"Mae"+i);
			list.add(pessoa);
		}		
		
		return list;
		
	}
	

	public static Pessoa criarPessoa(String nome,int cpf, String dataNascimento, String nomeMae){
		Pessoa pessoa = new Pessoa();
		pessoa.setNome(nome);
		pessoa.setCpf(cpf);
		pessoa.setDataNascimento(dataNascimento);
		pessoa.setNomeMae(nomeMae);
		
		return pessoa;
	}
	
	
	
	public void processar(){
		HashSet<Pessoa> listaPessoas = new HashSet<Pessoa>();
		ini = System.currentTimeMillis();
		listaPessoas = construirPessoa();
		
		 Enumeration<Pessoa> e = Collections.enumeration(listaPessoas);
	       	while (e.hasMoreElements()) {
				
				Pessoa pessoa =  e.nextElement();
				imp(pessoa);
				
			}
	    terminaContagem(0);
	}
	
	public boolean contains(String cpf){
		ini = System.currentTimeMillis();
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setCpf(Integer.parseInt(cpf));
		
		
		cpf = ""+pessoa1.hashCode(); 

		for (Pessoa pessoa: listaPessoas ) {
			
						
			if (cpf == ""+pessoa.getCpf()) {	
				
				terminaContagem(1);
				System.out.println("Achou CPF:"+cpf);
				return true;
				
			}
		}
		terminaContagem(1);
		System.out.println("Não Achou CPF:"+cpf);
		return false;
	}
	
	public void remover(String cpf){
		ini = System.currentTimeMillis();
		
		Enumeration<Pessoa> e = Collections.enumeration(listaPessoas);
	    
		while (e.hasMoreElements()) {	       		
	       	Pessoa pessoa =  e.nextElement();			       		
			if (cpf == ""+pessoa.getCpf()) {
				listaPessoas.remove(e);				
			}
			terminaContagem(2);	
		}

		
	}


	
	
	public static void imp(Pessoa pessoa){
		System.out.println("Nome: "+pessoa.getNome());
		System.out.println("CPF: "+pessoa.getCpf());
		System.out.println("Data Nascimento: "+pessoa.getDataNascimento());
		System.out.println("Nome da Mae: "+pessoa.getNomeMae());
		System.out.println("***************");	
		
	}
	
	public static void impTot(){
		System.out.println("\n-----------");
		System.out.println("HashSet");
		System.out.println("-----------");
		System.out.println("Adição: "+totais[0]+" ms");
		System.out.println("Contains: "+totais[1]+" ms");
		System.out.println("Remover: "+totais[2]+" ms");
		System.out.println("-----------");
	}
	
	public static void terminaContagem(int n){
		tempoTotal = System.currentTimeMillis() - ini;
		totais[n]=tempoTotal;
	}

}
